/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.bahmnisms.task;

import java.util.Date;

import org.apache.log4j.Logger;
import org.openmrs.api.PatientService;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.bahmnisms.api.BahmnismsService;
import org.springframework.beans.factory.annotation.Autowired;

import org.openmrs.scheduler.tasks.AbstractTask;

/**
 * This class configured as controller using annotation and mapped with the URL of
 * 'module/${rootArtifactid}/${rootArtifactid}Link.form'.
 */

public class BahmniSmsFollowupVisitTask extends AbstractTask {
	
	/** Logger for this class and subclasses */
	private Logger log = Logger.getLogger(BahmniSmsFollowupVisitTask.class);
	
	@Autowired
	UserService userService;
	
	PatientService patientService;
	
	/** Success form view name */
	private final String VIEW = "/module/${rootArtifactid}/${rootArtifactid}";
	
	public BahmniSmsFollowupVisitTask() {
		log.info("BahmniSmsFollowupVisitTask created at " + new Date());
	}
	
	@Override
	public void execute() {
		try {
			log.info("executing BahmniSmsFollowupVisitTask");
			//super.startExecuting();
			BahmnismsService bahmniSmsService = Context.getService(BahmnismsService.class);
			
			patientService = Context.getPatientService();
			bahmniSmsService.processFollowupVisitPatients(patientService);
		}
		catch (Exception e) {
			log.error("ERROR executing BahmniSmsFollowupVisitTask: " + e.getMessage());
		}
	}
	
	@Override
	public void shutdown() {
		log.info("shutting down BahmniSmsFollowupVisitTask");
		super.stopExecuting();
	}
}
