/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.bahmnisms.api.dao;

import java.util.List;
import java.util.Locale;

import org.hibernate.Query;
import org.openmrs.Obs;
import org.openmrs.Visit;
import org.openmrs.api.ConceptNameType;
import org.openmrs.api.context.Context;
import org.openmrs.api.db.hibernate.DbSession;
import org.openmrs.api.db.hibernate.DbSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("BahmnismsDao")
public class BahmnismsDao {
	
	//@Autowired
	DbSessionFactory sessionFactory;
	
	public void setSessionFactory(DbSessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Autowired
	public BahmnismsDao(DbSessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	private DbSession getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	/* public Visit getLatestVisit(String patientUuid, Integer locationId) {
		String queryString = "select v.* from obs o join encounter enc on o.encounter_id = enc.encounter_id join visit v on v.visit_id = enc.visit_id "
		        + "join person p on p.person_id = o.person_id where p.uuid=:patientUuid and v.voided = 0 and v.location_id=:locationId "
		        + "order by v.date_started desc";
		Query queryToGetVisitId = getSession().createQuery(queryString);
		queryToGetVisitId.setString("patientUuid", patientUuid);
		queryToGetVisitId.setInteger("locationId", locationId);
		queryToGetVisitId.setMaxResults(1);
		return (Visit) queryToGetVisitId.uniqueResult();
		
	    "select obs\n" +
	    "from Obs obs join obs.encounter enc join enc.visit v \n" +
	    "where obs.voided = false and obs.concept.conceptId in " +
	    "   ( select cs.concept.conceptId\n" +
	    "     from ConceptName cn, ConceptSet cs\n" +
	    "     where cs.conceptSet.conceptId = cn.concept.conceptId and cn.conceptNameType='FULLY_SPECIFIED' " +
	    "	  and cn.locale = :locale and cn.name=:conceptName)\n" +
	    "   and obs.person.uuid=:patientUuid and v.visitId =:visitId order by enc.encounterId desc";

	} */
	
	public Visit getLatestFollowupVisit(String patientUuid, Integer locationId) {
		String queryString = "select v from Obs o join o.encounter enc join enc.visit v "
		        + "where v.patient.uuid = :patientUuid and v.location.locationId = :locationId "
		        + "order by v.startDatetime desc";
		Query queryToGetVisitId = getSession().createQuery(queryString);
		queryToGetVisitId.setString("patientUuid", patientUuid);
		queryToGetVisitId.setInteger("locationId", locationId.intValue());
		queryToGetVisitId.setMaxResults(1);
		return (Visit) queryToGetVisitId.uniqueResult();
	}
	
	public Visit getLatestVisit(String patientUuid) {
		String queryString = "select v from Obs o join o.encounter enc join enc.visit v "
		        + "where v.patient.uuid = :patientUuid " + "order by v.startDatetime desc";
		Query queryToGetVisitId = getSession().createQuery(queryString);
		queryToGetVisitId.setString("patientUuid", patientUuid);
		queryToGetVisitId.setMaxResults(1);
		return (Visit) queryToGetVisitId.uniqueResult();
	}
	
	public Obs getLatestVisitFolloupDateObs(String patientUuid, Integer visitId, String followupConceptName, Locale locale) {
		String queryString = "select obs " + "from Obs obs join obs.encounter enc join enc.visit v "
		        + "where obs.voided = false and obs.concept.conceptId in " + "   (select cs.concept.conceptId "
		        + "     from ConceptName cn, ConceptSet cs"
		        + "     where cs.conceptSet.conceptId = cn.concept.conceptId and cn.conceptNameType='FULLY_SPECIFIED' "
		        + "	  and cn.locale = :locale and cn.name=:conceptName)"
		        + "   and obs.person.uuid=:patientUuid and v.visitId =:visitId order by enc.encounterId desc";
		Query queryToGetVisitFolloupDateObs = getSession().createQuery(queryString);
		queryToGetVisitFolloupDateObs.setString("patientUuid", patientUuid);
		queryToGetVisitFolloupDateObs.setInteger("visitId", visitId);
		queryToGetVisitFolloupDateObs.setString("conceptName", followupConceptName);
		queryToGetVisitFolloupDateObs.setMaxResults(1);
		return (Obs) queryToGetVisitFolloupDateObs.uniqueResult();
	}
	
	public Obs getLatestObsFor(String patientUuid, String conceptName) {
		Query queryToGetObservation = sessionFactory.getCurrentSession().createQuery(
		    "select obs " + " from Obs as obs, ConceptName as cn " + " where obs.person.uuid = :patientUuid "
		            + " and cn.concept = obs.concept.conceptId " + " and cn.name = (:conceptName) "
		            + " and cn.locale = :locale " + " and cn.conceptNameType = :conceptNameType "
		            + " and cn.voided = false " + " and obs.voided = false" + " order by obs.obsDatetime desc ");
		
		queryToGetObservation.setMaxResults(1);
		queryToGetObservation.setString("patientUuid", patientUuid);
		queryToGetObservation.setParameter("conceptName", conceptName);
		queryToGetObservation.setParameter("conceptNameType", ConceptNameType.FULLY_SPECIFIED);
		queryToGetObservation.setString("locale", Context.getLocale().getLanguage());
		
		return (Obs) queryToGetObservation.uniqueResult();
	}
	
}
